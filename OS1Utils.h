//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_OS1UTILS_H
#define OS1DRIVER_OS1UTILS_H
#include <string>
#include <vector>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#define MAX_READ_BUF_SIZE 16*1024 // Max per packet 16*1024
#define LIDAR_PACKET_SIZE 16*1024
#define IMU_PACKET_SIZE 48 //Bytes
#define AZIMUTH_BLOCK_SIZE 788 // Bytes
#define DATA_BLOCK_SIZE 12 // bytes
#define DATA_BLOCKS_PER_COLUMN 64 // int
#define AZIMUTH_BLOCK_COUNT 16 //int
#define ENCODER_TICKS_PER_REV 90112

namespace OS1{
    struct imu_data {
        std::string imu_id;
        uint64_t diagnostic_time;
        uint64_t accel_time;
        uint64_t gyro_time;
        float accel_x;
        float accel_y;
        float accel_z;
        float ang_rot_x;
        float ang_rot_y;
        float ang_rot_z;
    };

    struct sensor_info {
        std::string udp_ip;
        std::string lidar_port;
        std::string imu_port;
        std::string lidar_mode;
        std::vector<double> beam_altitude_angles;
        std::vector<double> beam_azimuth_angles;
    };

    class point_cloud_frame{
    public:
        point_cloud_frame(pcl::PointCloud<pcl::PointXYZ>::Ptr cld){
            full = false;
            curr_point_num = 0;
            cloud = cld;
        };
        ~point_cloud_frame(){
            cloud = nullptr;
        }
        bool full;
        int curr_point_num = 0;
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
    };
}

#endif //OS1DRIVER_OS1UTILS_H
