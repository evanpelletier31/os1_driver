//
// Created by Evan on 2020-06-25.
//

#ifndef OS1DRIVER_OS1UDPCLIENT_H
#define OS1DRIVER_OS1UDPCLIENT_H
#include <string>
#include <cstring>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unordered_map>
#include "OS1Utils.h"
#include "OS1Sensor.h"
#include "OS1DataHandler.h"


namespace OS1 {
    class OS1UdpClient {
    public:
        OS1UdpClient(std::unordered_map<std::string, OS1Sensor *> *sensors_objs);
        ~OS1UdpClient();
        void pollSockets();
    private:
        std::unordered_map<std::string, OS1Sensor *> *sensor_objs;
        int createSocket(const std::string& port_number);
        int lidar_fd;
        int imu_fd;
    };
}

#endif //OS1DRIVER_OS1UDPCLIENT_H
