//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_SENSORINTERFACE_H
#define OS1DRIVER_SENSORINTERFACE_H

#include "OS1UdpClient.h"
#include "OS1Sensor.h"
#include <string>
#include <thread>
#include <mutex>


namespace OS1 {
    class SensorInterface {
    public:
        explicit SensorInterface(std::string host_ip);
        void addSensor(const std::string& sensor_ip, const std::string& horizontal_res,
                       const std::string& frequency);
        void removeSensor(const std::string& sensor_ip);
        void setSensorMode(const std::string& sensor_id, const std::string& horizontal_res,
                const std::string& frequency);
        void setSensorAzimuthWindow(const std::string& sensor_id, const std::string& start_angle, const std::string& stop_angle);
        pcl::PointCloud<pcl::PointXYZ>::Ptr getPointCloudFrame();
        void startSensors();
    private:
        std::mutex m;
        OS1UdpClient *udp_cli;
        std::string host_ip; // Ip for all sensors to send UDP data to
        std::unordered_map<std::string, OS1Sensor *> *sensors; // Map to hold active sensors
        OS1Sensor *getSensor(const std::string& sensor_id);

        void *pollLoop();
    };

}

#endif //OS1DRIVER_SENSORINTERFACE_H
