//
// Created by Evan on 2020-06-26.
//

#include "OS1DataHandler.h"


/*
 * Function to convert buffer to xyz points for pointcloud
 */
void *OS1::OS1DataHandler::handleLidarData(uint8_t *buf, OS1::OS1Sensor *sensor){
    for(int azblc = 0; azblc < AZIMUTH_BLOCK_COUNT; azblc++){
        const uint8_t *azimuth_block = get_azimuth_block(azblc, buf);
        if(sensor->frames->isEmpty()){
            add_frame(sensor);
        }
        if (sensor->frames->rear()->curr_point_num > sensor->points_in_cloud) {
            sensor->frames->pop();
            add_frame(sensor);
        }
        if (sensor->frames->rear()->curr_point_num == sensor->points_in_cloud){
            sensor->frames->rear()->full = true; // Point cloud has been filled
            add_frame(sensor);
        }

        point_cloud_frame *newest_frame = sensor->frames->rear();
        int curr_frame_point = newest_frame->curr_point_num;
        for(int point_id = 0; point_id < DATA_BLOCKS_PER_COLUMN; point_id++){
            if (newest_frame->curr_point_num + point_id > sensor->points_in_cloud){
                break;
            }
            double r =  get_pixel_range(point_id, azimuth_block)/1000.0;
            if (r == 0){ // Skip calculation if not needed
                newest_frame->cloud->points[point_id + curr_frame_point].x = 0;
                newest_frame->cloud->points[point_id + curr_frame_point].x = 0;
                newest_frame->cloud->points[point_id + curr_frame_point].x = 0;
                continue;
            }
            double theta = get_encoder_angle(azimuth_block) + sensor->trig_table[point_id][2];
            newest_frame->cloud->points[point_id + curr_frame_point].x = r * cos(theta) *
                    sensor->trig_table[point_id][0]; // r*cos(theta)*cos(phi)
            newest_frame->cloud->points[point_id + curr_frame_point].y = -r *
                    sin(theta) * sensor->trig_table[point_id][0]; // -r*sin(theta)*cos(phi)
            newest_frame->cloud->points[point_id + curr_frame_point].z = r *
                    sensor->trig_table[point_id][1]; // r*sin(phi)
        }
        if (newest_frame->curr_point_num < sensor->points_in_cloud) {
            newest_frame->curr_point_num += DATA_BLOCKS_PER_COLUMN;
        }
    }
    delete(buf);
}

void OS1::OS1DataHandler::add_frame(OS1::OS1Sensor *sensor) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr new_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    new_cloud->height = 1;
    new_cloud->width = sensor->points_in_cloud;
    new_cloud->points.resize(new_cloud->height*new_cloud->width);
    sensor->frames->push(new point_cloud_frame(new_cloud));
}

