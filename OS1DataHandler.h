//
// Created by Evan on 2020-06-26.
//

#ifndef OS1DRIVER_OS1DATAHANDLER_H
#define OS1DRIVER_OS1DATAHANDLER_H
#include "OS1DataHandler.h"
#include "OS1Utils.h"
#include "OS1Sensor.h"
#include "packet.h"
#include "SharedQueue.h"
namespace OS1 {
    class OS1DataHandler {
    public:
        static void *handleLidarData(uint8_t *buf, OS1Sensor *sensor);
    private:
        static void add_frame(OS1Sensor *sensor);
    };
}

#endif //OS1DRIVER_OS1DATAHANDLER_H
