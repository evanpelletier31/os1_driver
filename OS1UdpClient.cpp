//
// Created by Evan on 2020-06-25.
//

#include "OS1UdpClient.h"
#define LIDAR_PORT "7502"
#define IMU_PORT "7503"


OS1::OS1UdpClient::OS1UdpClient(std::unordered_map<std::string, OS1Sensor *> *sensors) {
    sensor_objs = sensors; // map of references to all sensors
    lidar_fd = createSocket(LIDAR_PORT);
    imu_fd = createSocket(IMU_PORT);
    if (lidar_fd == -1){
        throw std::runtime_error("Failed to connect to lidar");
    }else if(imu_fd == -1){
        throw std::runtime_error("Failed to connect to IMU");
    }
}

OS1::OS1UdpClient::~OS1UdpClient() {
    close(lidar_fd);
    close(imu_fd);
}

int OS1::OS1UdpClient::createSocket(const std::string& port_number) {
    // generate addrinfo struct
    struct addrinfo hints{}, *res, *ai;
    struct sockaddr addr_info{};
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    // populate struct
    int ret = getaddrinfo(nullptr, port_number.c_str(), &hints, &res);
    // check for error, print to terminal
    if (ret!=0){
        std::cerr << "getaddrinfo(): " << gai_strerror(ret) << std::endl;
        return -1;
    }
    // iterate through LL, try to bind
    int socket_fd;
    for(ai = res; ai != nullptr; ai = ai->ai_next){
        socket_fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        int bind_res = bind(socket_fd, ai->ai_addr, ai->ai_addrlen);
        if (socket_fd < 0 || bind_res <0){
            close(socket_fd);
            continue; // Try again if failed
        }
        break; // break once connection is made
    }
    freeaddrinfo(res);
    fcntl(socket_fd, F_SETFL, O_NONBLOCK); // Set to non blocking
    return socket_fd; // return a file descriptor
}

void OS1::OS1UdpClient::pollSockets() {

    fd_set read_fds;
    FD_ZERO(&read_fds);
    // add both file descriptors to set
    FD_SET(lidar_fd, &read_fds);
    FD_SET(imu_fd, &read_fds);
    //TODO:CHECK FOR ERROR WILL RAISE 139 IF NOT ABLE TO CONNECT, EX NETCAT
    struct timeval timeout{ // Timeout value, seconds
            timeout.tv_sec = 1,
            timeout.tv_usec = 0
    };
    int num_fds = std::max(lidar_fd, imu_fd) + 1;
    int ret = select(num_fds, &read_fds, NULL, NULL, &timeout);
    if (ret > 0) {


        if (FD_ISSET(lidar_fd, &read_fds)) { // lidar fd ready
            // Do something with the lidar data
            auto *buf = (uint8_t *) malloc(LIDAR_PACKET_SIZE); // Create new buffer for packet
            sockaddr_in cli{};
            socklen_t len = sizeof(cli);
            ssize_t msg_len = recvfrom(lidar_fd, (char *) buf, LIDAR_PACKET_SIZE, 0, (struct sockaddr *) &cli, &len);
            if (msg_len == -1) {
                std::cerr << "Lidar packet: " << std::strerror(errno) << std::endl; // Unexpected packet length
            }
            // Get the Ip address of the incoming packet (os1 ip)
            char ip[16];
            inet_ntop(AF_INET, &cli.sin_addr, ip, sizeof(ip));
            OS1Sensor *sensor = sensor_objs->find(std::string(ip))->second; // Get a reference to the sensor
            OS1::OS1DataHandler::handleLidarData(buf, sensor);
        }
        if (FD_ISSET(imu_fd, &read_fds)) { // imu fd ready
            // Do something with the imu data
            std::cout;
        }
    }
}


