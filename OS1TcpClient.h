//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_OS1TCPCLIENT_H
#define OS1DRIVER_OS1TCPCLIENT_H
#include <string>
#include <iostream>
#include <netdb.h>
#include <sys/socket.h>
#include <memory>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include "OS1Utils.h"

namespace OS1 {
    class OS1TcpClient {
    public:
        explicit OS1TcpClient(std::string sensor_ip);
        ~OS1TcpClient();
        std::string sendCommand(const std::string& command) const;
    private:
        int createSocket();
        std::string host_ip;
        int fd;
    };
}
#endif //OS1DRIVER_OS1TCPCLIENT_H
