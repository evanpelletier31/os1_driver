//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_OS1DRIVER_H
#define OS1DRIVER_OS1DRIVER_H
#include <string>
#include <utility>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include "SensorInterface.h"


namespace OS1 {
    class OS1Driver {
    private:
        SensorInterface *sensor_interface;
    public:
        /*
         * Create an Interface for all sensors
         * @param host_address: IPV4 address of the host, usually the ethernet device's address
         */
        void createInterface(std::string host_address) {
            sensor_interface = new SensorInterface(std::move(host_address));
        }

        /*
         * Add a new sensor to the interface
         * @param sensor_address: IPV4 address of the OS1 sensor being added
         * @param horizontal_resolution: number of vertical beams per revolution, can be {512, 1024, 2048}
         * @param frequency: Number of frames per second, can be {10, 20}
         * @param is_OS1_16: true for OS1-16 false for OS1-64
         */
        void addSensor(const std::string &sensor_address, const std::string &horizontal_resolution = "1024",
                       const std::string &frequency = "10") {
            if (sensor_interface == nullptr) {
                throw std::runtime_error("Must Create an Interface with createInterface before calling other methods");
            }
            sensor_interface->addSensor(sensor_address, horizontal_resolution, frequency);
        }

        /*
         * Remove a sensor from map of sensors
         * @param sensor_id: IPV4 address of the sensor to remove
         */
        void removeSensor(const std::string &sensor_id) {
            if (sensor_interface == nullptr) {
                throw std::runtime_error("Must Create an Interface with createInterface before calling other methods");
            }
            sensor_interface->removeSensor(sensor_id);
        }

        /*
         * Set the mode of a specific sensor after adding it
         * @param sensor_id: IPV4 address of the target sensor
         * @param horizontal_resolution: number of vertical beams per revolution, can be {512, 1024, 2048}
         * @param frequency: Number of frames per second, can be {10, 20}
         */
        void setSensorMode(const std::string &sensor_id, const std::string &horizontal_resolution,
                           const std::string &frequency) {
            sensor_interface->setSensorMode(sensor_id, horizontal_resolution, frequency);
        }

        /*
         * Set the window at which the sensor takes measurements, start and stop angles are in degrees
         */
        void setSensorAzimuthWindow(const std::string &sensor_id, const std::string &start_angle,
                                    const std::string &stop_angle) {
            sensor_interface->setSensorAzimuthWindow(sensor_id, start_angle, stop_angle);
        }

        /*
         * Start all sensors that have been added, this will
         */
        void startScan() {
            sensor_interface->startSensors();
        }

        /*
         * Stop all sensors, stop saving data
         */
        void stopScan() {

        }

        /*
         * Get a single Point cloud representing one frame
         */
        pcl::PointCloud<pcl::PointXYZ>::Ptr getFrame() {
            return sensor_interface->getPointCloudFrame();
        }
    };
}

#endif //OS1DRIVER_OS1DRIVER_H
