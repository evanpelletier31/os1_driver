//
// Created by Evan on 2020-06-24.
//


#include "OS1Sensor.h"
#define MAX_QUEUE_SIZE 2

OS1::OS1Sensor::OS1Sensor(const std::string& host_ip, const std::string& sensor_ip, const std::string& horizontal_res,
                          const std::string& frequency) {
    this->points_in_cloud = 64 * std::stoi(horizontal_res);
    tcp_client = new OS1TcpClient(sensor_ip); // Create a new tcp connection
    getBeamIntrinsics(); // get the beam info
    createTrigTable(); // get trig table for point conversion, speed up conversion
    setUdpIp(host_ip); // Make sure sensor is streaming to correct address
    frames = new SharedQueue<point_cloud_frame *>;
    frames->setMaxQueueSize(MAX_QUEUE_SIZE);
}

OS1::OS1Sensor::~OS1Sensor() {
    delete(tcp_client);
}

void OS1::OS1Sensor::setMode(const std::string& horizontal_res, const std::string& frequency) {
    points_in_cloud = 64 * std::stoi(horizontal_res);
    if (horizontal_res == "512" || horizontal_res == "1024" || horizontal_res == "2048"){
        if(frequency == "10" || frequency == "20"){
            if (horizontal_res == "2048" && frequency != "10") {// Only invalid combination of modes
                //TODO:Set mode via tcp_client
                std::string mode_cmd = "set_config_param lidar_mode " + horizontal_res + "x" + frequency;
                tcp_client->sendCommand(mode_cmd); // Send the command
                reinitialize();
                return;
            }
        }
    }
    throw std::runtime_error("Invalid Mode!");
}

void OS1::OS1Sensor::setAzimuthWindow(const std::string& start_angle, const std::string& stop_angle) {
    //TODO: set via tcp_client
    if (std::stoi(start_angle) > 360 || std::stoi(stop_angle) > 360 || std::stoi(start_angle) > std::stoi(stop_angle)){
        throw std::runtime_error("Invalid mode, make sure start_angle is smaller than stop_angle. Make sure"
                                 "both angles are positive and less than 360");
    }
    // Valid arguments for Azimuth Window range from 0->36000, so multiply by 100
    std::string start = std::to_string(std::stoi(start_angle)*100);
    std::string stop = std::to_string(std::stoi(stop_angle)*100);
    std::string azimuth_cmd = "set_config_param azimuth_window [" + start + "," + stop + "]";
    tcp_client->sendCommand(azimuth_cmd);
    reinitialize();
}

void OS1::OS1Sensor::reinitialize() const {
    tcp_client->sendCommand("reinitialize");
}

void OS1::OS1Sensor::setUdpIp(const std::string& ip) {
    tcp_client->sendCommand("set_config_param udp_ip " + ip);
    reinitialize();
}

void OS1::OS1Sensor::createTrigTable() {
    for(int i = 0; i < beam_azimuth_angles.size(); i++){
        std::array<float, 3> trig_entry{};
        trig_entry[0] = cos(beam_altitude_angles[i] * M_PI / 180); // Needed for xy
        trig_entry[1] = sin(beam_altitude_angles[i] * M_PI / 180); // Needed for yz
        trig_entry[2] = M_PI * beam_azimuth_angles[i] / 180; // Needed to get theta
        trig_table.push_back(trig_entry);
    }
}

void OS1::OS1Sensor::getBeamIntrinsics() {
    std::string return_data = tcp_client->sendCommand("get_beam_intrinsics"); // request os1 info
    Json::Value root{};
    Json::Reader reader;
    reader.parse(return_data, root);
    std::string default_val = "failed json parse";
    for (const auto& altitude_angle: root["beam_altitude_angles"]) {
        beam_altitude_angles.push_back(altitude_angle.asDouble()); // append angle to vector
    }
    for (const auto& azimuth_angle: root["beam_azimuth_angles"]) {
        beam_azimuth_angles.push_back(azimuth_angle.asDouble()); // append angle to vector
    }
}
