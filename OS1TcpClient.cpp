//
// Created by Evan on 2020-06-24.
//

#include "OS1TcpClient.h"

#include <utility>

OS1::OS1TcpClient::OS1TcpClient(std::string host_ip) {
    this->host_ip = std::move(host_ip);
    fd = createSocket();
    if (fd == -1){
        throw std::runtime_error("Failed To create TCP File Descriptor");
    }
}

OS1::OS1TcpClient::~OS1TcpClient() {
    close(fd);
}

int OS1::OS1TcpClient::createSocket() {
    std::cout << "Creating tcp socket" << std::endl;
    // Create hint structure to connect to server
    struct addrinfo hints{}, *res, *ai;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    int ret = getaddrinfo(host_ip.c_str(), "7501", &hints, &res); // Populate struct with getaddrinfo
    if (ret != 0) {
        std::cerr << "getaddrinfo: " << gai_strerror(ret) << std::endl; // Get error msg
        throw std::runtime_error("Failed to connect to OS1, make sure connection is good and "
                                 " correct hostname is provided");
    }
    int socket_fd;
    for (ai = res; ai != nullptr; ai = ai->ai_next){ // Iterate through ai (LL) and find os1
        socket_fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        int con_status = connect(socket_fd, ai->ai_addr, ai->ai_addrlen);
        if (socket_fd < 0 || con_status < 0){
            close(socket_fd);
            continue; // move on, try next connection
        }
        break; // Break once connection is made
    }
    freeaddrinfo(res);
    std::cout << "Socket Succesfully Created" << std::endl;
    return socket_fd;
}

    std::string OS1::OS1TcpClient::sendCommand(const std::string& command) const{
        std::stringstream cmd;
        cmd << command.c_str() << "\n";
        std::string formatted_cmd = cmd.str();
        ssize_t len = write(fd, formatted_cmd.c_str(), formatted_cmd.length()); // Send to os1
        if (len != (size_t)formatted_cmd.length()){
            throw std::runtime_error("Failed to send command, make sure correct os1 ip is provided");
        }
        std::stringstream read_output;
        auto read_buf = std::unique_ptr<char[]>{new char[MAX_READ_BUF_SIZE + 1]};
        while (true){ // Start reading the response
            len = read(fd, read_buf.get(), MAX_READ_BUF_SIZE);
            if (len < 0){
                throw std::runtime_error("Failed to receive command");
            }
            read_buf.get()[len] = '\0';
            read_output << read_buf.get();
            if (read_buf.get()[len-1] == '\n'){
                break; // Break once full string has been received
            }
        }
        // return the output
        std::string response = read_output.str();
        if (response.find("error") != std::string::npos){
            throw std::runtime_error("Invalid tcp command: " + cmd.str());
        }
        return response.erase(response.find_last_not_of(" \r\n\t")+1);
    }

