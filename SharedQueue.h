//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_SHAREDQUEUE_H
#define OS1DRIVER_SHAREDQUEUE_H
#include <queue>
#include <mutex>


namespace OS1{
    template <typename t>
    class SharedQueue{
    private:
        std::queue<t> _queue;
        int max_queue_size = 100; // Max number of items that can be in queue
    public:
        std::mutex m;
        ~SharedQueue(){
            for(int i = 0; i < _queue.size(); i++){
                _queue.pop();
            };
        }

        int size(){
            return _queue.size();
        }

        void push(t data){ // override std queue push
            m.lock();
            if(_queue.size() > max_queue_size){ // Remove oldest item if full
                this->remove_front();
            }
            _queue.push(data);
            m.unlock();
        }

        t pop(){ // override std queue pop
            m.lock();
            if (_queue.empty()){
                return nullptr;
            }
            t tmp = _queue.front();
            _queue.pop();
            m.unlock();
            return tmp;
        }

        void setMaxQueueSize(int size){ // Set the max size of the queue
            max_queue_size = size;
        }

        t front(){
            if (_queue.empty()){
                return nullptr;
            }
            return _queue.front();
        }
        bool isEmpty(){
            return _queue.empty();
        }
        t rear(){
            return _queue.back(); // Return the newest element
        }
        void remove_front(){
            if (_queue.empty()){
                return;
            }
            //delete(_queue.front());
            _queue.pop();
        }
    };
}

#endif //OS1DRIVER_SHAREDQUEUE_H
