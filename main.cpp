#include <iostream>
#include "SensorInterface.h"
#include "OS1Driver.h"
#include <mutex>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/cloud_viewer.h>

int main() {
    OS1::OS1Driver cli = OS1::OS1Driver();
    cli.createInterface("169.254.7.210");
    cli.addSensor("169.254.159.151");
    cli.startScan();
   //while(true){
       //cli.getFrame();
      // continue;
    //}
    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->initCameraParameters ();
    while (!viewer->wasStopped ()) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cld = cli.getFrame();
        if (cld != nullptr){
            viewer->removeAllPointClouds();
            viewer->addPointCloud(cld, "frame");
            viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "frame");
        }
        viewer->spinOnce(50);
    }
    return 1;
}
