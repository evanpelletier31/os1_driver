//
// Created by Evan on 2020-06-26.
//

#ifndef OS1DRIVER_PACKET_H
#define OS1DRIVER_PACKET_H
#include <sys/types.h>
#include <cstring>
#include <sys/types.h>
#include <cmath>
#include <iostream>
#include "OS1Utils.h"


namespace OS1 {

    // These functions are used to retrieve data from a raw lidar packet, argument "column_number" MUST be between
    // 0 and 16. Argument "pixel_num" MUST be between 0 and 64. These functions provide no error checks, undesired
    // data WILL be returned if failing to stay within the listed ranges

    // Get the nth azimuth block from a packet
    inline const u_int8_t *get_azimuth_block(int column_num, const u_int8_t *packet_buffer){
        return packet_buffer + (column_num * AZIMUTH_BLOCK_SIZE);
    }
    inline u_int8_t *get_data_block(int block_num,const u_int8_t *azimuth_block){
        return const_cast<u_int8_t *>(azimuth_block + 16 + (block_num * DATA_BLOCK_SIZE));
    }
    // Get the timestamp from an azimuth block
    inline u_int64_t get_timestamp(const u_int8_t *azimuth_block){
        u_int64_t timestamp;
        memcpy(&timestamp, azimuth_block, sizeof(timestamp));
        return timestamp;
    }
    // Get the measurement id from an azimuth block (offset 8 bytes)
    inline u_int16_t get_measurement_id(const u_int8_t *azimuth_block){
        u_int16_t measurement_id;
        memcpy(&measurement_id, azimuth_block + 8, sizeof(measurement_id));
        return  measurement_id;
    }
    // Get the frame id from an azimuth block (offset 10 bytes)
    inline u_int16_t get_frame_id(const u_int8_t *azimuth_block){
        u_int16_t frame_id;
        memcpy(&frame_id, azimuth_block + 10, sizeof(frame_id));
        return frame_id;
    }
    // Get the encoder count from an azimuth block (offset 12 bytes)
    inline u_int32_t get_encoder_count(const u_int8_t *azimuth_block){
        u_int32_t encoder_ticks;
        memcpy(&encoder_ticks, azimuth_block + 12, sizeof(encoder_ticks));
        return encoder_ticks;
    }
    inline float get_encoder_angle(const uint8_t *azimuth_block){
        uint32_t ticks;
        memcpy(&ticks, azimuth_block +12, sizeof(ticks));
        return (2.0 * M_PI * ticks / (float)ENCODER_TICKS_PER_REV);
    }
    // Get the range in mm, must specify which pixel (between 0 and 64)
    inline u_int32_t get_pixel_range(int pixel_num, const u_int8_t *azimuth_block){
        u_int32_t range;
        u_int8_t *d_block = get_data_block(pixel_num, azimuth_block);
        memcpy(&range, d_block, sizeof(range));
        range &= 0x000fffff;
        return range;
    }
    // Get the reflectivity of a pixel, must specify which pixel (between 0 and 64)
    inline u_int16_t get_pixel_reflectivity(int pixel_num, const u_int8_t *azimuth_block){
        u_int16_t reflectivity;
        u_int8_t *d_block = get_data_block(pixel_num, azimuth_block);
        memcpy(&reflectivity, d_block + 4, sizeof(reflectivity));
        return reflectivity;
    }
    // Get the intensity of a pixel, must specify which pixel (between 0 and 64)
    inline u_int16_t get_pixel_intensity(int pixel_num, const u_int8_t *azimuth_block){
        u_int16_t intensity;
        u_int8_t *d_block = get_data_block(pixel_num, azimuth_block);
        memcpy(&intensity, d_block + 6, sizeof(intensity));
        return intensity;
    }
    // Get the ambient noise of a pixel, must specify which pixel (between 0 and 64)
    inline u_int16_t get_pixel_noise(int pixel_num, const u_int8_t *azimuth_block){
        u_int16_t noise;
        u_int8_t *d_block = get_data_block(pixel_num, azimuth_block);
        memcpy(&noise, d_block+ 8, sizeof(noise));
        return noise;
    }
    // Get the status of an azimuth block
    inline u_int32_t get_azimuth_status(const u_int8_t *azimuth_block){
        u_int32_t status;
        memcpy(&status, azimuth_block + 16 + (DATA_BLOCKS_PER_COLUMN * DATA_BLOCK_SIZE), sizeof(status));
        return status;
    }

    // These functions are to retrieve IMU data from a raw packet

    // Get IMU diagnostic time
    inline u_int64_t get_imu_diagnostic_time(const u_int8_t *imu_packet){
        u_int64_t diagnostic_time;
        memcpy(&diagnostic_time,imu_packet, sizeof(diagnostic_time));
        return diagnostic_time;
    }
    // Get accelerometer read time
    inline u_int64_t get_accel_read_time(const u_int8_t *imu_packet){
        u_int64_t accel_time;
        memcpy(&accel_time, imu_packet + 8, sizeof(accel_time));
        return accel_time;
    }
    // Get gyroscope read time
    inline u_int64_t get_gyro_read_time(const u_int8_t *imu_packet){
        u_int64_t gyro_time;
        memcpy(&gyro_time, imu_packet + 16, sizeof(imu_packet));
        return gyro_time;
    }
    // Get linear x acceleration in g
    inline float get_x_acceleration(const u_int8_t* imu_packet) {
        float x_accel;
        memcpy(&x_accel, imu_packet + 24, sizeof(x_accel));
        return x_accel;
    }
    // Get y acceleration in g
    inline float get_y_acceleration(const u_int8_t* imu_packet) {
        float y_accel;
        memcpy(&y_accel, imu_packet + 28, sizeof(y_accel));
        return y_accel;
    }
    // Get z acceleration in g
    inline float get_z_acceleration(const u_int8_t* imu_packet) {
        float z_accel;
        memcpy(&z_accel, imu_packet + 32, sizeof(z_accel));
        return z_accel;
    }
    // Get x angular rotation degrees/second
    inline float get_x_rotation(const u_int8_t* imu_packet) {
        float x_rot;
        memcpy(&x_rot, imu_packet + 36, sizeof(x_rot));
        return x_rot;
    }
    // Get y angular rotation degrees/second
    inline float get_y_rotation(const u_int8_t* imu_packet) {
        float y_rot;
        memcpy(&y_rot, imu_packet + 40, sizeof(y_rot));
        return y_rot;
    }
    // Get z angular rotation degrees/second
    inline float get_z_rotation(const u_int8_t* imu_packet) {
        float z_rot;
        memcpy(&z_rot, imu_packet + 44, sizeof(z_rot));
        return z_rot;
    }
}
#endif //OS1DRIVER_PACKET_H
