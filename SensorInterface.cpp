//
// Created by Evan on 2020-06-24.
//

#include "SensorInterface.h"

#include <utility>

OS1::SensorInterface::SensorInterface(std::string host_ip) {
    this->host_ip = std::move(host_ip);
    sensors = new std::unordered_map<std::string, OS1Sensor *>;
    udp_cli = new OS1UdpClient(sensors);
}
void OS1::SensorInterface::addSensor(const std::string& sensor_ip, const std::string& horizontal_res,
        const std::string& frequency) {
    sensors->insert({sensor_ip,new OS1Sensor(host_ip, sensor_ip, horizontal_res, frequency)}) ;
}

void OS1::SensorInterface::removeSensor(const std::string& sensor_ip) {
    delete(sensors->at(sensor_ip));
    sensors->erase(sensor_ip);
}

OS1::OS1Sensor *OS1::SensorInterface::getSensor(const std::string& sensor_id) {
    return sensors->at(sensor_id); //TODO:: CHeck if valid entry
}

void OS1::SensorInterface::setSensorMode(const std::string& sensor_id, const std::string& horizontal_res,
        const std::string& frequency) {
    getSensor(sensor_id)->setMode(horizontal_res,frequency);
}

pcl::PointCloud<pcl::PointXYZ>::Ptr OS1::SensorInterface::getPointCloudFrame() {
    for (const auto& frame: *sensors) {
        if (frame.second->frames->isEmpty()) { // If empty or not ready
            return nullptr;
        }else{
            if (frame.second->frames->front()->full) { // last frame is ready
                return frame.second->frames->pop()->cloud;
            }else {
                return nullptr;
            }
        }
    }
}

void OS1::SensorInterface::setSensorAzimuthWindow(const std::string& sensor_id, const std::string& start_angle,
        const std::string& stop_angle) {
    getSensor(sensor_id)->setAzimuthWindow(start_angle, stop_angle);
}

void OS1::SensorInterface::startSensors() {
    std::thread pollthread(&SensorInterface::pollLoop, this);
    pollthread.detach();
}

void *OS1::SensorInterface::pollLoop() {
    while(true) {
        udp_cli->pollSockets();
    }
}



