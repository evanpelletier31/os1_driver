//
// Created by Evan on 2020-06-24.
//

#ifndef OS1DRIVER_OS1SENSOR_H
#define OS1DRIVER_OS1SENSOR_H
#include <string>
#include <stdexcept>
#include "OS1TcpClient.h"
#include "SharedQueue.h"
#include "OS1Utils.h"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>
#include <cmath>

namespace OS1 {
    class OS1Sensor {
    public:
        OS1Sensor(const std::string& host_ip, const std::string& sensor_ip, const std::string& horizontal_res,
                const std::string& frequency);
        ~OS1Sensor();
        void setMode(const std::string& horizontal_res, const std::string& frequency);
        void setAzimuthWindow(const std::string& start_angle, const std::string& stop_angle);
        void reinitialize() const;
        void setUdpIp(const std::string& ip);
        std::vector<std::array<float,3>> trig_table;
        std::vector<double> beam_altitude_angles;
        std::vector<double> beam_azimuth_angles;
        int points_in_cloud;
        SharedQueue<point_cloud_frame *> *frames;
    private:
        std::string udp_dest; //Ip sensor will stream data to
        OS1TcpClient *tcp_client;
        void createTrigTable();
        void getBeamIntrinsics();
    };
}

#endif //OS1DRIVER_OS1SENSOR_H
